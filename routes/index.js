const express = require("express");
const router = express.Router();
const jobController = require("../controller/jobController");
const filterController = require("../controller/filterController");

module.exports = app => {
  router.get("/jobs/all", jobController.findAllJob);
  router.post("/jobs/store-json", jobController.insertData);
  router.get("/jobs/search", jobController.findJob);
  router.get("/jobs/filters/all", filterController.findAllFilter);
  router.post("/jobs/filters/store-json", filterController.insertData);

  app.use("/api", router);
};
