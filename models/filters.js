const mongoose = require("mongoose");
const filterSchema = mongoose.Schema({
  job_type: {
    type: Array,
    default: []
  },
  work_schedule: {
    type: Array,
    default: []
  },
  experience: {
    type: Array,
    default: []
  },
  department: {
    type: Array,
    default: []
  }
});

module.exports = mongoose.model("filters", filterSchema);
