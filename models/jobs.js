const mongoose = require("mongoose");
const jobSchema = mongoose.Schema({
  total_jobs_in_hospital: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  job_title: {
    type: String,
    required: true
  },
  items: {
    type: Array,
    default: []
  }
});

jobSchema.index({ name: "text", job_title: "text" });

module.exports = mongoose.model("jobs", jobSchema);
