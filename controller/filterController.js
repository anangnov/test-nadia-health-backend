"use-strict";

const Filters = require("../models/filters");
const json = require("../data/filters.json");

const findAllFilter = async (req, res) => {
  let data = await Filters.find({});

  let objectResponse = {
    error: false,
    message: "Found",
    data: data[0]
  };

  return req.output(req, res, objectResponse, "info", 200);
};

const insertData = async (req, res) => {
  let filter = new Filters({
    job_type: json.job_type,
    work_schedule: json.work_schedule,
    experience: json.experience,
    department: json.department
  });

  let objectResponse = {
    error: false,
    message: "Success"
  };

  filter.save(() => {
    return req.output(req, res, objectResponse, "info", 200);
  });
};

module.exports = { findAllFilter, insertData };
