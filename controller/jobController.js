"use-strict";

const Jobs = require("../models/jobs");
const json = require("../data/jobs.json");

const insertData = async (req, res) => {
  let tmp = json.length - 1;
  for (let i = 0; i < json.length; i++) {
    let job = new Jobs({
      total_jobs_in_hospital: json[i].total_jobs_in_hospital,
      name: json[i].name,
      job_title: json[i].job_title,
      items: json[i].items
    });

    job.save(() => {
      if (i === tmp) {
        let objectResponse = {
          error: false,
          message: "Success"
        };

        return req.output(req, res, objectResponse, "info", 200);
      }
    });
  }
};

const findAllJob = async (req, res) => {
  let data = await Jobs.find({});

  let objectResponse = {
    error: false,
    message: "Found",
    data: data
  };

  return req.output(req, res, objectResponse, "info", 200);
};

const findJob = async (req, res) => {
  Jobs.createIndexes();
  let data = await Jobs.find({
    $text: {
      $search: req.query.job_title
    }
  });

  let objectResponse = {
    error: false,
    message: data.length == 0 ? "Not Found" : "Found",
    count: data.length,
    data: data
  };

  return req.output(req, res, objectResponse, "info", 200);
};

module.exports = { insertData, findAllJob, findJob };
